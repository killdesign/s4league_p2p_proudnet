// dllmain.cpp : Definiert den Einstiegspunkt f�r die DLL-Anwendung.
#include "stdafx.h"

BOOL APIENTRY DllMain( HMODULE hModule, DWORD  reason, LPVOID lpReserved )
{
	if( reason == DLL_PROCESS_ATTACH )
	{
		DisableThreadLibraryCalls( hModule );

		g_P2P = new CP2P();
	}

	if( reason == DLL_PROCESS_DETACH )
	{
		delete g_P2P;
	}

	return TRUE;
}

