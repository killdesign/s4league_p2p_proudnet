#include "stdafx.h"
#define READABLE (PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY | PAGE_READONLY | PAGE_READWRITE | PAGE_WRITECOPY)

CTools g_Tools;


CTools::CTools(void)
{
}

CTools::~CTools(void)
{
}

UINT CTools::MemoryscanInt( UINT startaddress, UINT size, int value, int timeout )
{
	MEMORY_BASIC_INFORMATION info;
	clock_t time = clock();
	UINT scanBaseaddress;

	do
	{
		scanBaseaddress = startaddress;
		VirtualQuery((LPCVOID)startaddress, &info, sizeof(info));

		while( scanBaseaddress <= (startaddress + size) )
		{
			if( scanBaseaddress >= ((UINT)info.BaseAddress + info.RegionSize) )
			{
				VirtualQuery( (LPCVOID)scanBaseaddress, &info, sizeof(info) );
			}

			if( info.Protect & READABLE && info.State == MEM_COMMIT && info.Type != MEM_PRIVATE)
			{
				for( UINT i = scanBaseaddress; i <= ((UINT)info.BaseAddress + info.RegionSize - 4); i+=4)
				{
					if( *(DWORD*)(i) == value )
					{
						return i;
					}
				}
			}
			scanBaseaddress = ((UINT)info.BaseAddress + info.RegionSize);
		}
	}
	while( (clock() - time) <= timeout );

	return 0;
}