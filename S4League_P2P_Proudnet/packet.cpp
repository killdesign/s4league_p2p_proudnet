#include "stdafx.h"

CPacket::CPacket( void* buffer )
{
	m_buffer = (DWORD)buffer;
	m_index = 0;
	m_headerSize = 0;
	m_size = 0;

	if( *(WORD*)(buffer) == __MAGIC )
	{
		m_index += 2;
		m_headerSize += 2;
		switch( *(BYTE*)((DWORD)buffer+2) )
		{
		case 1:
			m_index += 2;
			m_headerSize += 2;
			m_size = *(BYTE*)((DWORD)buffer+3);
			break;
		case 2:
			m_index += 3;
			m_headerSize += 3;
			m_size = *(WORD*)((DWORD)buffer+3);
			break;
		case 4:
			m_index += 5;
			m_headerSize += 5;
			m_size = *(DWORD*)((DWORD)buffer+3);
			break;
		//default:
		}
	}
	else
	{
		//Log output. Its not a vaid Proudnet Packet, since no Magic
	}
}


CPacket::~CPacket(void)
{
}

void CPacket::setIndex( int index )
{
	m_index = index;
}

void CPacket::skip( int nNum )
{
	m_index += nNum;
}