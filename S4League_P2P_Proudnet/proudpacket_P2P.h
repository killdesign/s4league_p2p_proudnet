#pragma once
struct SProudnetheader_P2P {
	WORD magic; //Magic denk ich mal. 0xABCD | 0xABCE
	BYTE unkn1; //Ka. Evtl. flag?
	BYTE unkn2; //Ka. Evtl. flag?
	DWORD payloadLen; //L�nge des Payloads
	WORD sequence; //Sequenz halt
	WORD unkn3; //Ka was das ist. Vermutung: Hash des Packets.
	DWORD reserved; //Denk mal ist reserved. Ist immer 0
};

class CProudpacket_P2P
{
private:
public:
	SProudnetheader_P2P *header;
	std::vector<CPacket*> bodys;
	
	CProudpacket_P2P( );
	CProudpacket_P2P( void* buffer );
	~CProudpacket_P2P(void);
};

