#pragma once


#include "stdafx.h"


class CConsole
{
public:
	CConsole();
	~CConsole();

	void log(BYTE bColor, const char* pcSys, const char* pcFmt, ...);


private:


	// Lock
	bool m_Lock;

	// Console handle
	HANDLE m_hConsole;

	// MSG Mutex
	CRITICAL_SECTION m_mMsg;

	// Logfiles
	FILE* m_pConsoleLogfile; // Console log file
	FILE* m_pLogfile;		 // Sys log file
	char m_pcLogfile[128];	 // Sys log file path

	// Time
	SYSTEMTIME m_Time;

	// Msg itself
	char m_pcMsg[1024];

	// Methods
	void adjustColor(BYTE bColor);
};
